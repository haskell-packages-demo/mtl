{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE Strict #-}


import Prelude
    ( ($), (++)
    , Int
    , IO
    , length
    , putStrLn
    , show
    , String
    )

import Control.Monad.Reader
    ( asks
    , local
    , Reader
    , runReader
    )


calculateContentLen :: Reader String Int
calculateContentLen = do asks length

-- Calls calculateContentLen after adding a prefix to the Reader content.
calculateModifiedContentLen :: Reader String Int
calculateModifiedContentLen = local ("Prefix " ++) calculateContentLen

main :: IO ()
main = do
    let s = "12345";
    let modifiedLen = runReader calculateModifiedContentLen s
    let len = runReader calculateContentLen s
    putStrLn $ "Modified 's' length: " ++ show modifiedLen
    putStrLn $ "Original 's' length: " ++ show len
