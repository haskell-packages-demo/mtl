{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE Safe #-}
{-# LANGUAGE Strict #-}
-- {-# LANGUAGE Trustworthy #-}

import Prelude
    ( ($), (++), (==)
    , Bool
    , Int
    , IO
    , print
    , putStr
    , return
    , show
    , String
    )

import Control.Monad.Reader
    ( ask
    , asks
    , Reader
    , runReader
    )
import Data.Map (Map)
import Data.Map qualified as Map
    ( fromList
    , lookup
    , size
    )
import Data.Maybe (fromMaybe)


type Bindings = Map String Int;

-- The Reader monad, which implements this complicated check.
calcIsCountCorrect :: Reader Bindings Bool
calcIsCountCorrect = do
    count <- asks (lookupVar "count")
    bindings <- ask
    return (count == Map.size bindings)
  where
    -- The selector function to  use with 'asks'.
    -- Returns value of the variable with specified name.
    lookupVar :: String -> Bindings -> Int
    lookupVar name bindings = fromMaybe 0 (Map.lookup name bindings)

sampleBindings :: Bindings
sampleBindings = Map.fromList [("count",3), ("1",1), ("b",2)]

main :: IO ()
main = do
    putStr $ "Count is correct for bindings " ++ show sampleBindings ++ ": ";
    print (runReader calcIsCountCorrect sampleBindings);
    -- Returns True if the "count" variable contains correct bindings size.
