{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE Safe #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE Trustworthy #-}

import Control.Monad.Reader
    ( ask
    , asks
    , Reader
    , runReader
    ) -- Foundation.Monad.Reader?

import Foundation
    ( ($), (==), (<>)
    , Bool
    , fromMaybe
    , Int
    , IO
    , putStr
    , putStrLn
    , return
    , Show
    , show
    , String
    )

import RIO
    (
    -- String -- !!?
    )
import RIO.Map (Map)
import RIO.Map qualified as Map
    ( fromList
    , lookup
    , size
    )
-- ^ https://www.stackage.org/haddock/nightly-2022-05-14/foundation-0.0.28/Foundation-Collection.html#v:lookup

print :: Show a => a -> IO ()
print x =  putStrLn (show x)
-- ^ from standard Prelude
-- https://www.stackage.org/haddock/lts-19.6/base-4.15.1.0/src/System.IO.html#print

type Bindings = Map String Int;

-- The Reader monad, which implements this complicated check.
calcIsCountCorrect :: Reader Bindings Bool
calcIsCountCorrect = do
    count <- asks (lookupVar "count")
    bindings <- ask
    return (count == Map.size bindings)
  where
    -- The selector function to  use with 'asks'.
    -- Returns value of the variable with specified name.
    lookupVar :: String -> Bindings -> Int
    lookupVar name bindings = fromMaybe 0 (Map.lookup name bindings)

sampleBindings :: Bindings
sampleBindings = Map.fromList [("count",3), ("1",1), ("b",2)]

main :: IO ()
main = do
    putStr $ "Count is correct for bindings " <> show sampleBindings <> ": ";
    -- putStrLn (show (runReader calcIsCountCorrect sampleBindings));
    print (runReader calcIsCountCorrect sampleBindings);
    -- Returns True if the "count" variable contains correct bindings size.
