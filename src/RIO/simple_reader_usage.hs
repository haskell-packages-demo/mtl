{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE Safe #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE Trustworthy #-}


import RIO
    ( ($), (==), (<>)
    , Bool
    , displayShow
    , fromMaybe
    , Int
    , IO
    , logInfo
    , return
    , runSimpleApp
    , String -- !!?
    )
import RIO.Map (Map)
import RIO.Map qualified as Map
    ( fromList
    , lookup
    , size
    )
-- import RIO.Prelude.Display
-- import RIO.Prelude.Display.Display

import Control.Monad.Reader
    ( ask
    , asks
    , Reader
    , runReader
    )


type Bindings = Map String Int;

-- The Reader monad, which implements this complicated check.
calcIsCountCorrect :: Reader Bindings Bool
calcIsCountCorrect = do
    count <- asks (lookupVar "count")
    bindings <- ask
    return (count == Map.size bindings)
  where
    -- The selector function to  use with 'asks'.
    -- Returns value of the variable with specified name.
    lookupVar :: String -> Bindings -> Int
    lookupVar name bindings = fromMaybe 0 (Map.lookup name bindings)

sampleBindings :: Bindings
sampleBindings = Map.fromList [("count",3), ("1",1), ("b",2)]

main :: IO ()
main = runSimpleApp $ do
    logInfo $ "Count is correct for bindings " <> displayShow sampleBindings <> ": ";
    logInfo (displayShow (runReader calcIsCountCorrect sampleBindings));
    -- Returns True if the "count" variable contains correct bindings size.
