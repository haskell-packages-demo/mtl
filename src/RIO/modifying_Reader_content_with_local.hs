{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Strict #-}
{-# LANGUAGE Trustworthy #-}


import RIO
    ( ($), (++), (<>)
    , display
    , Int
    , IO
    , length
    , logInfo
    , runSimpleApp
    , String
    )

import Control.Monad.Reader
    ( asks
    , local
    , Reader
    , runReader
    )


calculateContentLen :: Reader String Int
calculateContentLen = do asks length

-- Calls calculateContentLen after adding a prefix to the Reader content.
calculateModifiedContentLen :: Reader String Int
calculateModifiedContentLen = local ("Prefix " ++) calculateContentLen

main :: IO ()
main = runSimpleApp $ do
    let
        s :: String
        s = "12345"
    let modifiedLen = runReader calculateModifiedContentLen s
    let len = runReader calculateContentLen s
    logInfo $ "Modified 's' length: " <> display modifiedLen
    logInfo $ "Original 's' length: " <> display len
